export class Patient {

  constructor(
    public name: string,
    public age: number,
    public bloodPressure: {sys: number, dia: number},
    public heartRate: number
  ) {  }

}
