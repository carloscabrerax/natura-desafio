import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {Patient} from '../patient';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.css']
})
export class InfoComponent implements OnInit {
  public patients: Patient[] = [{
    name: 'Carlos Cabrera', age: 23, bloodPressure: {sys: 120, dia: 80 }, heartRate: 70
  }];
  private newPatient: Patient;

  add(name, age, sys, dia, heart) {
    this.newPatient = new Patient( '', 0, {sys: 0, dia: 0}, 70);
    this.newPatient.name = name.value;
    this.newPatient.age = age.value;
    this.newPatient.bloodPressure.sys = sys.value;
    this.newPatient.bloodPressure.dia = dia.value;
    this.newPatient.heartRate = heart.value;
    this.patients.push(this.newPatient);
    name.value = '';
    age.value = '';
    sys.value = '';
    dia.value = '';
    heart.value = '';
    name.focus();
    return false;
  }

  edit(patient) {
    console.log(patient.value);
  }

  delete(patient) {
    this.patients.splice(this.patients.indexOf(patient), 1);
  }

  ngOnInit() {
  }
}

